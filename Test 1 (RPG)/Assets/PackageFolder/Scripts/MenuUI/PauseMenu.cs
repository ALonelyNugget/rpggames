﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject LevelMenu;
    bool MenuOpen = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) && MenuOpen == false)
        {
            LevelMenu.SetActive(true);
            MenuOpen = true;
            Debug.Log("Pause");
            Pause();

        }
        else if (Input.GetKeyDown(KeyCode.P) && MenuOpen == true)
        {
            LevelMenu.SetActive(false);
            MenuOpen = false;
            Debug.Log("UnPause");
            Unpause();

        }
    }
    void Pause()
    {
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.None;
    }
    void Unpause()
    {
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
    }
}
