﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaBar : MonoBehaviour
{
	GameObject StaminaBarObj;
	Slider slider;

	void Start()
	{

		StaminaBarObj = GameObject.FindWithTag("StaminaBar");
		slider = StaminaBarObj.GetComponent<Slider>();


	}
	public void SetMaxStamina(float Stamina)
	{
		slider.maxValue = Stamina;
		slider.value = Stamina;

	}

	public void SetStamina(float Stamina)
	{
		slider.value = Stamina;
	}
}
