﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class StatPanel : MonoBehaviour
{
    public GameObject statPanel;
    public DummyHealthScript PlayerHPScript;
    public bool StatMenuOpen = false;
    public TextMeshProUGUI PlayerLevel;
    public TextMeshProUGUI RequiredEXP;
    public TextMeshProUGUI TotalEXP;

    public TextMeshProUGUI MaxHP;
    public TextMeshProUGUI CurrentHP;

    public TextMeshProUGUI MaxStam;
    public TextMeshProUGUI CurrentStam;

    public TextMeshProUGUI MaxMana;
    public TextMeshProUGUI CurrentMana;

    public TextMeshProUGUI PhysicalRes;
    public TextMeshProUGUI MagicRes;
    public TextMeshProUGUI StatusRes;

    public TextMeshProUGUI IsBleeding;
    public TextMeshProUGUI TotalBleedDamage;
    public TextMeshProUGUI BleedFrequency;

    public TextMeshProUGUI IsPoisoned;
    public TextMeshProUGUI TotalPoisonDamage;
    public TextMeshProUGUI PoisonFrequency;


    CharacterStatScipt CharScript;
    private void Start()
    {
        CharScript = gameObject.GetComponent<CharacterStatScipt>();
        //level
        PlayerLevel = PlayerLevel.GetComponent<TextMeshProUGUI>();
        RequiredEXP = RequiredEXP.GetComponent<TextMeshProUGUI>();
        TotalEXP = TotalEXP.GetComponent<TextMeshProUGUI>();
        //health
        MaxHP = MaxHP.GetComponent<TextMeshProUGUI>();
        CurrentHP = CurrentHP.GetComponent<TextMeshProUGUI>();
        //stamina
        MaxStam = MaxStam.GetComponent<TextMeshProUGUI>();
        CurrentStam = CurrentStam.GetComponent<TextMeshProUGUI>();
        //mana
        MaxMana = MaxMana.GetComponent<TextMeshProUGUI>();
        CurrentMana = CurrentMana.GetComponent<TextMeshProUGUI>();
        //Resistences
        PhysicalRes = PhysicalRes.GetComponent<TextMeshProUGUI>();
        MagicRes = MagicRes.GetComponent<TextMeshProUGUI>();
        StatusRes = StatusRes.GetComponent<TextMeshProUGUI>();
        //Bleed
        IsBleeding = IsBleeding.GetComponent<TextMeshProUGUI>();
        TotalBleedDamage = TotalBleedDamage.GetComponent<TextMeshProUGUI>();
        BleedFrequency = BleedFrequency.GetComponent<TextMeshProUGUI>();
        //Poison
        IsPoisoned = IsPoisoned.GetComponent<TextMeshProUGUI>();
        TotalPoisonDamage = TotalPoisonDamage.GetComponent<TextMeshProUGUI>();
        PoisonFrequency = PoisonFrequency.GetComponent<TextMeshProUGUI>();


    }
    private void Update()
    {
        if (StatMenuOpen)
        {
            statPanel.SetActive(true);
        }
        else if (StatMenuOpen == false)
        {
            statPanel.SetActive(false);
        }

        //EXP
        PlayerLevel.text = CharScript.PlayerLevel.ToString();
        RequiredEXP.text = CharScript.LevelUpXP.ToString();
        TotalEXP.text = CharScript.CurrentXP.ToString();
        //HP
        MaxHP.text = PlayerHPScript.MaxHealthPoints.ToString();
        CurrentHP.text = PlayerHPScript.CurrentHealthPoints.ToString();
        //Stamina
        MaxStam.text = PlayerHPScript.MaxStaminaPoints.ToString();
        CurrentStam.text = PlayerHPScript.CurrentStaminaPoints.ToString();
        //Mana
        MaxMana.text = PlayerHPScript.MaxManaPoints.ToString();
        CurrentMana.text = PlayerHPScript.CurrentManaPoints.ToString();
        //Resistences
        PhysicalRes.text = PlayerHPScript.PhysicalResistanceModifier.ToString();
        MagicRes.text = PlayerHPScript.MagicResistanceModifier.ToString();
        StatusRes.text = PlayerHPScript.StatusResistanceModifier.ToString();
        //Bleed
        IsBleeding.text = PlayerHPScript.isBleeding.ToString();
        TotalBleedDamage.text = PlayerHPScript.TotalBleedDamage.ToString();
        BleedFrequency.text = PlayerHPScript.BleedFrequency.ToString();
        //Poison
        IsPoisoned.text = PlayerHPScript.isPoisoned.ToString();
        TotalPoisonDamage.text = PlayerHPScript.TotalPoisonDamage.ToString();
        PoisonFrequency.text = PlayerHPScript.PoisonFrequency.ToString();

    }

    public void ToggleStatMenu()
    {
        if (StatMenuOpen)
        StatMenuOpen = false;
        else if (StatMenuOpen == false)
        StatMenuOpen = true;
    }


}
