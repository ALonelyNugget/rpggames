﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{

	GameObject HealthBarObj;
	Slider slider;


	void Start()
	{

		HealthBarObj = GameObject.FindWithTag("HealthBar");
		slider = HealthBarObj.GetComponent<Slider>();

	}
	public void SetMaxHealth(float health)
	{
		slider.maxValue = health;
		slider.value = health;
	}

    public void SetHealth(float health)
	{
		slider.value = health;

	}

}
