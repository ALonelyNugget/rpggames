﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;



public class CharacterStatScipt : MonoBehaviour
{
    /* NotePad to do list
    Author - Luke Saliba
    Author notes
    well using this package, if the user would like to use the UI Provided, the + and - Buttons Must take
    the scriptboard as an object then each one need a allocation or de-allocation depending on the stat




    */
    [Tooltip("Players Level")]
    public int PlayerLevel = 1; // Player Level
    [Tooltip("EXP Required for level up")]
    public float LevelUpXP = 400;
    [Tooltip("Players current EXP")]
    public float CurrentXP = 0;
    


    [Space]
    [Tooltip("Players Strength")]
    public int PlayerStrength = 9; // Strength based weapon damage // 
    [Tooltip("Players Dexterity")]
    public int PlayerDexterity = 9; // Finess based weapon damage //Crit Chance // dodge chance
    [Tooltip("Players Inteligence")]
    public int PlayerIntelligence = 9; // Magic Damage // Max Mana - 10 per point
    [Tooltip("Players Wisdom")]
    public int PlayerWisdom = 9; // Mana Regen // Magic Resist(elemental resitences) 
    [Tooltip("Players Constitution")]
    public int PlayerConstitution = 9; // Max Health - 10 per point // Max stamina - 10 per point
    [Tooltip("Players Endurance")]
    public int PlayerEndurance = 9; // Physiscal Resist // Status Resistence (Eg: poison, bleed ect) // max Stamina - 20 per point // stamina Regen - N/A

    int UnalocatedStatPoints; // 2 stat points per level can be alocated freely 
    int AllocatedStrengthPoints;
    int AllocatedDexterityPoints;
    int AllocatedIntelegencePoints;
    int AllocatedWisdomPoints;
    int AllocatedConsitutionPoints;
    int AllocatedEndurancePoints;
    int TotalAllocatedStatPoints;

    [Space]

    //[HideInInspector]
    public float MaxHealth;

    //[HideInInspector]
    public float MaxMana;

    //[HideInInspector]
    public float MaxStamina;


    [HideInInspector]
    public float MagicResistence;
    [HideInInspector]
    public float PhysicalResistence;
    [HideInInspector]
    public float StatusResistence;
    [Tooltip("Percentage CritChance")]
    public float CritChance;



    [Space]

    //these are for the stat displays in each level menu section
    //drag and drop the relevent stat displays into the correct sections
    [Tooltip("Inside Level Menu, under Strength, grab Str - Stat Display and drop it here")]
    public TextMeshProUGUI StrDisplay;
    [Tooltip("Inside Level Menu, under Dexterity, grab Dex - Stat Display and drop it here")]
    public TextMeshProUGUI DexDisplay;
    [Tooltip("Inside Level Menu, under Intelegence, grab Int - Stat Display and drop it here")]
    public TextMeshProUGUI IntDisplay;
    [Tooltip("Inside Level Menu, under Wisdom, grab Wis - Stat Display and drop it here")]
    public TextMeshProUGUI WisDisplay;
    [Tooltip("Inside Level Menu, under Constitution, grab Con - Stat Display and drop it here")]
    public TextMeshProUGUI ConDisplay;
    [Tooltip("Inside Level Menu, under Endurance, grab End - Stat Display and drop it here")]
    public TextMeshProUGUI EndDisplay;
    public TextMeshProUGUI UnallocatedPoints;
    


    public HealthBar healthBar;
    public StaminaBar staminaBar;
    void Start()
    {


        UpdateStatPoints();

    }


    void Update()
    {
        LevelUp();
        if (Input.GetKeyDown(KeyCode.I))
        {
            Debug.Log(PlayerStrength + " Strength");
            Debug.Log(PlayerDexterity + " Dexterity");
            Debug.Log(PlayerIntelligence + " Intelligence");
            Debug.Log(PlayerWisdom + " Wisdom");
            Debug.Log(PlayerConstitution + " Constition");
            Debug.Log(PlayerEndurance + " Endurance");
            Debug.Log(UnalocatedStatPoints + " Current Spare Points");
        }
    }
    public void AllocateStr()
    {
        if (UnalocatedStatPoints >= 1 && !Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedStrengthPoints += 1;
            Debug.Log("Stat Point Allocated");
            UpdateStatPoints();
        }
        else if (UnalocatedStatPoints >= 10 && Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedStrengthPoints += 10;
            Debug.Log("Stat Point Allocated");
            UpdateStatPoints();
        }
        else
        {
            Debug.Log("Failed To Allocate Point");

        }
    }
    public void AllocateDex()
    {
        if (UnalocatedStatPoints >= 1 && !Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedDexterityPoints += 1;
            Debug.Log("Stat Point Allocated");
            UpdateStatPoints();
        }
        else if (UnalocatedStatPoints >= 10 && Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedDexterityPoints += 10;
            Debug.Log("Stat Point Allocated");
            UpdateStatPoints();
        }
        else
        {
            Debug.Log("Failed To Allocate Point");

        }
    }
    public void AllocateInt()
    {
        if (UnalocatedStatPoints >= 1 && !Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedIntelegencePoints += 1;
            Debug.Log("Stat Point Allocated");
            UpdateStatPoints();
        }
        else if (UnalocatedStatPoints >= 10 && Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedIntelegencePoints += 10;
            Debug.Log("Stat Point Allocated");
            UpdateStatPoints();
        }
        else
        {
            Debug.Log("Failed To Allocate Point");

        }
    }
    public void AllocateWis()
    {
        if (UnalocatedStatPoints >= 1 && !Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedWisdomPoints += 1;
            Debug.Log("Stat Point Allocated");
            UpdateStatPoints();
        }
        else if (UnalocatedStatPoints >= 10 && Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedWisdomPoints += 10;
            Debug.Log("Stat Point Allocated");
            UpdateStatPoints();
        }
        else
        {
            Debug.Log("Failed To Allocate Point");

        }
    }
    public void AllocateCon()
    {
        if (UnalocatedStatPoints >= 1 && !Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedConsitutionPoints += 1;
            Debug.Log("Stat Point Allocated");
            UpdateStatPoints();

        }
        else if (UnalocatedStatPoints >= 10 && Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedConsitutionPoints += 10;
            Debug.Log("Stat Point Allocated");
            UpdateStatPoints();
        }
        else
        {
            Debug.Log("Failed To Allocate Point");

        }
    }
    public void AllocateEnd()
    {
        if (UnalocatedStatPoints >= 1 && !Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedEndurancePoints += 1;
            Debug.Log("Stat Point Allocated");
            UpdateStatPoints();
        }
        else if (UnalocatedStatPoints >= 10 && Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedEndurancePoints += 10;
            Debug.Log("Stat Point Allocated");
            UpdateStatPoints();
        }
        else
        {
            Debug.Log("Failed To Allocate Point");
        }
    }
    public void DeAllocateStr()
    {
        if ((PlayerStrength >= 10 + PlayerLevel) && !Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedStrengthPoints -= 1;
            Debug.Log("Stat Point Removed");
            UpdateStatPoints();
        }
        else if ((PlayerStrength >= 19 + PlayerLevel) && Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedStrengthPoints -= 10;
            Debug.Log("10 Stat Points Removed");
            UpdateStatPoints();
        }
        else
        {
            Debug.Log("Failed To Remove Point");

        }
    }
    public void DeAllocateDex()
    {
        if ((PlayerDexterity >= 10 + PlayerLevel) && !Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedDexterityPoints -= 1;
            Debug.Log("Stat Point Removed");
            UpdateStatPoints();
        }
        else if ((PlayerDexterity >= 19 + PlayerLevel) && Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedDexterityPoints -= 10;
            Debug.Log("Stat Point Removed");
            UpdateStatPoints();
        }
        else
        {
            Debug.Log("Failed To Remove Point");

        }
    }
    public void DeAllocateInt()
    {
        if ((PlayerIntelligence >= 10 + PlayerLevel) && !Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedIntelegencePoints -= 1;
            Debug.Log("Stat Point Removed");
            UpdateStatPoints();
        }
        else if ((PlayerIntelligence >= 19 + PlayerLevel) && Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedIntelegencePoints -= 10;
            Debug.Log("10 Stat Points Removed");
            UpdateStatPoints();
        }
        else
        {
            Debug.Log("Failed To Remove Point");

        }
    }
    public void DeAllocateWis()
    {
        if ((PlayerWisdom >= 10 + PlayerLevel) && !Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedWisdomPoints -= 1;
            Debug.Log("Stat Point Removed");
            UpdateStatPoints();
        }
        else if ((PlayerWisdom >= 19 + PlayerLevel) && Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedWisdomPoints -= 10;
            Debug.Log("10 Stat Points Removed");
            UpdateStatPoints();
        }
        else
        {
            Debug.Log("Failed To Remove Point");

        }
    }
    public void DeAllocateCon()
    {
        if ((PlayerConstitution >= 10 + PlayerLevel) && !Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedConsitutionPoints -= 1;
            Debug.Log("Stat Point Removed");
            UpdateStatPoints();
        }
        else if ((PlayerConstitution >= 19 + PlayerLevel) && Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedConsitutionPoints -= 10;
            Debug.Log("10 Stat Points Removed");
            UpdateStatPoints();
        }
        else
        {
            Debug.Log("Failed To Remove Point");

        }
    }
    public void DeAllocateEnd()
    {
        if ((PlayerEndurance >= 10 + PlayerLevel) && !Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedEndurancePoints -= 1;
            Debug.Log("Stat Point Removed");
            UpdateStatPoints();
        }
        else if ((PlayerEndurance >= 19 + PlayerLevel) && Input.GetKey(KeyCode.LeftShift))
        {
            AllocatedEndurancePoints -= 10;
            Debug.Log("10 Stat Points Removed");
            UpdateStatPoints();
        }
        else
        {
            Debug.Log("Failed To Remove Point");
        }
    }

    public void UpdateStatPoints()
    {
        



        PlayerStrength = 9 + PlayerLevel + AllocatedStrengthPoints;
        PlayerDexterity = 9 + PlayerLevel + AllocatedDexterityPoints;
        PlayerIntelligence = 9 + PlayerLevel + AllocatedIntelegencePoints;
        PlayerWisdom = 9 + PlayerLevel + AllocatedWisdomPoints;
        PlayerConstitution = 9 + PlayerLevel + AllocatedConsitutionPoints;
        PlayerEndurance = 9 + PlayerLevel + AllocatedEndurancePoints;
        TotalAllocatedStatPoints = AllocatedStrengthPoints + AllocatedDexterityPoints + AllocatedIntelegencePoints + AllocatedWisdomPoints + AllocatedConsitutionPoints + AllocatedEndurancePoints;
        UnalocatedStatPoints = (PlayerLevel * 2) - TotalAllocatedStatPoints;



        MaxHealth = PlayerConstitution * 20.0f;
        MaxMana = PlayerIntelligence * 10.0f;
        MaxStamina = (PlayerEndurance * 20.0f) + (PlayerConstitution * 10.0f);
        PhysicalResistence = PlayerEndurance / 5.0f;
        StatusResistence = PlayerEndurance / 10.0f;
        MagicResistence = PlayerWisdom / 5.0f;



        CritChance = (PlayerDexterity * 0.15f);

        
        StrDisplay = StrDisplay.GetComponent<TextMeshProUGUI>();
        DexDisplay = DexDisplay.GetComponent<TextMeshProUGUI>();
        IntDisplay = IntDisplay.GetComponent<TextMeshProUGUI>();
        WisDisplay = WisDisplay.GetComponent<TextMeshProUGUI>();
        ConDisplay = ConDisplay.GetComponent<TextMeshProUGUI>();
        EndDisplay = EndDisplay.GetComponent<TextMeshProUGUI>();
        UnallocatedPoints = UnallocatedPoints.GetComponent<TextMeshProUGUI>();

        StrDisplay.text = PlayerStrength.ToString();
        DexDisplay.text = PlayerDexterity.ToString();
        IntDisplay.text = PlayerIntelligence.ToString();
        WisDisplay.text = PlayerWisdom.ToString();
        ConDisplay.text = PlayerConstitution.ToString();
        EndDisplay.text = PlayerEndurance.ToString();
        UnallocatedPoints.text = UnalocatedStatPoints.ToString();
    }
    public void LevelUp()
    {
        if (CurrentXP >= LevelUpXP)
        {
            

            CurrentXP -= LevelUpXP;
            PlayerLevel++;
            LevelUpXP *= 2; 
            UpdateStatPoints();
            healthBar.SetMaxHealth(MaxHealth);
            staminaBar.SetMaxStamina(MaxStamina);
        }
    }

}