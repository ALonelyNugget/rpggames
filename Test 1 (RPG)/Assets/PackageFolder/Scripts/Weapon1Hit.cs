﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon1Hit : MonoBehaviour
{

    [Header("Weapon Base Attack Damage")]
    [Tooltip("Base Attack Damage of Weapon")]
    public float WeaponDamage = 20;

    [Space]
    [Header("Weapon Stat Type & Modifier")]

    [Tooltip("Which stat type your weapon is")]
    public bool StrBased = false;
    [Tooltip("Str Based Weapons, stat Modifier (* 100 for %)")]
    public float StrMod = 0.6f;
    [Tooltip("Which stat type your weapon is")]

    public bool DexBased = false;
    [Tooltip("Dex Based Weapons, stat Modifier (* 100 for %)")]
    public float DexMod = 0.4f;

    [Tooltip("Which stat type your weapon is")]
    public bool StrDexBased = false;
    [Tooltip("Str & Dex (Combination) Based Weapons, stat Modifier (* 100 for %), Combo Type Deals This Percentage of each stat")]
    public float StrDexMod = 0.25f;

    [Tooltip("Which stat type your weapon is")]
    public bool IntBased = false;
    [Tooltip("Int Based Weapons, stat Modifier (* 100 for %)")]
    public float IntMod = 0.8f;

    //Variable for dealing damage
    float DamageDelt = 0;

    [Header("Weapon Status Afflictions")]
    [Space]
    //AfflictBleed
    [Tooltip("Will Your Attack Inflict Bleed")]
    public bool AfflictBleed = false;
    [Tooltip("How Often The Enemy will Take Damage in seconds")]
    public float BleedFrequency = 3.0f;
    [Tooltip("Amount of Bleed Damage intervals")]
    public int RoundsBleeding = 4;
    [Tooltip("Percentage Amound of Max Damage That will Be Inflicted as Bleed")]
    public float BleedModifier = 0.2f;
    float BleedDamage;

    [Space]
    //AfflictPoison
    [Tooltip("Will Your Attack Inflict poison")]
    public bool AfflictPoison = false;
    [Tooltip("Total Damage Poison Will Deal")]
    public float PoisonDamage = 20;
    [Tooltip("How Often The Enemy will Take Damage in seconds")]
    public float PoisonFrequency = 0.05f;
    [Tooltip("Amount of Poison Damage intervals")]
    public int RoundsPoisoned = 400;

    [Space]
    //Damage Bonus
    public bool Magic = false;

    [Space]
    [HideInInspector]
    public CharacterStatScipt PlayerStats;
    DummyHealthScript Enemy;
   
    private void OnTriggerEnter(Collider other)
    {
        Enemy = other.GetComponent<DummyHealthScript>();
        
        if (other.tag != "Enemy")
        {
            return;
        }
        if (StrBased == true)
        {
            float damage = 0;
            damage = WeaponAttackStr();
            if (CheckCrit())
            {
                Enemy.TakeDamage(damage * 2);
                Debug.Log((damage * 2) + " Attempted Damage Delt");
            }

            else
            {
                Enemy.TakeDamage(damage);
                Debug.Log(damage + " Attempted Damage Delt");
            }

        }
        if (DexBased == true)
        {
            float damage = 0;
            damage = WeaponAttackDex();
            if (CheckCrit())
            {
                Enemy.TakeDamage(damage * 2);
                Debug.Log((damage * 2) + " Attempted Damage Delt");
            }
            else
            {
                Enemy.TakeDamage(damage);
                Debug.Log(damage + " Attempted Damage Delt");
            }
        }
        if (StrDexBased == true)
        {
            float damage = 0;
            damage = WeaponAttackStrDex();
            if (CheckCrit())
            {
                Enemy.TakeDamage(damage * 2);
                Debug.Log((damage * 2) + " Attempted Damage Delt");
            }
            else
            {
                Enemy.TakeDamage(damage);
                Debug.Log(damage + " Attempted Damage Delt");
            }
        }
        if (IntBased == true)
        {
            WeaponAttackInt();
            Enemy.TakeDamage(DamageDelt);
            //magic weapon
            //Debug.Log(WeaponDamage + " WeaponDamage " + "+ " + (PlayerStats.PlayerIntelligence * 0.8) + " Magic Damage");
            Debug.Log(DamageDelt + " Attempted Damage Delt");
        }

        CheckAfflictedStatus();
    }
    void CheckAfflictedStatus()
    {
        if (AfflictBleed == true)
        {
            BleedDamage = DamageDelt * BleedModifier;
            if (Enemy.isBleeding == false)
            {
                Enemy.TotalBleedDamage += BleedDamage;
            }
            else if (Enemy.isBleeding)
            {
                Enemy.StackBleed();
                Enemy.TotalBleedDamage += BleedDamage;

            }
            Enemy.BleedFrequency = BleedFrequency;
            Enemy.RoundsBleeding = RoundsBleeding;
            Enemy.isBleeding = true;
        }
        else
        {
            Debug.Log("No Bleed Status Attack");
        }
        if (AfflictPoison == true)
        {
            
            if (Enemy.isPoisoned == false)
            {
                Enemy.TotalPoisonDamage += PoisonDamage;
            }
            else if (Enemy.isPoisoned)
            {
                Enemy.StackPoison();
                Enemy.TotalPoisonDamage += PoisonDamage;

            }
            Enemy.PoisonFrequency = PoisonFrequency;
            Enemy.RoundsPoisoned = RoundsPoisoned;
            Enemy.isPoisoned = true;
        }
        else
        {
            Debug.Log("No Poison Status Attack");
        }
    }

    float WeaponAttackStr()
    {
        float AttackMod;
        AttackMod = PlayerStats.PlayerStrength * StrMod;
        DamageDelt = WeaponDamage + AttackMod;
        return DamageDelt;
    }
    float WeaponAttackDex()
    {
        float AttackMod;
        AttackMod = PlayerStats.PlayerDexterity * DexMod;
        DamageDelt = WeaponDamage + AttackMod;
        return DamageDelt;
    }
    float WeaponAttackStrDex()
    {
        float AttackMod;
        AttackMod = (PlayerStats.PlayerStrength * StrDexMod) + (PlayerStats.PlayerDexterity * StrDexMod);
        DamageDelt = WeaponDamage + AttackMod;
        return DamageDelt;
    }
    float WeaponAttackInt()
    {
        float AttackMod;
        AttackMod = PlayerStats.PlayerIntelligence * IntMod;
        DamageDelt = WeaponDamage + AttackMod;
        return DamageDelt;
    }
    bool CheckCrit()
    {
        float Rand;
        Rand = Random.Range(0.0f, 100.0f);
        Debug.Log(Rand + " Number Rolled " + PlayerStats.CritChance + " Current CritChance");
        
        return (Rand <= PlayerStats.CritChance);


    }

}
