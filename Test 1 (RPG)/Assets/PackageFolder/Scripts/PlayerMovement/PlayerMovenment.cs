﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovenment : MonoBehaviour
{
    public float playerSpeed = 8;
    public float sprintSpeed = 12;
    public float jumpHeight = 6;
    public bool IsSprinting = false;
    public bool CanSprint = true;
    CharacterController cc;
    Vector3 velocity;

    void Start()
    {

        cc = GetComponent<CharacterController>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) && CanSprint)
        {
            Sprinting();
            
        }
        else
        {
            Walking();
            
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }

        
    }
    public void Jump()
    {
        if (cc.isGrounded)
        velocity.y = jumpHeight;
    }
    public void Walking()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move;
        move = transform.right * x + transform.forward * z;

        cc.Move(move * playerSpeed * Time.deltaTime);
        if (cc.isGrounded)
        {
            if (Input.GetKeyDown("1"))
            {
                velocity.y = jumpHeight;
            }
        }

        velocity += Physics.gravity * Time.fixedDeltaTime;

        cc.Move(velocity * Time.deltaTime);
        IsSprinting = false;
    }
    public void Sprinting()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move;
        move = transform.right * x + transform.forward * z;

        cc.Move(move * sprintSpeed * Time.deltaTime);
        if (cc.isGrounded)
        {
            if (Input.GetKeyDown("1"))
            {
                velocity.y = jumpHeight;
            }
        }

        velocity += Physics.gravity * Time.fixedDeltaTime;

        cc.Move(velocity * Time.deltaTime);
        IsSprinting = true;
    }

}
