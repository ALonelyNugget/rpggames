﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyHealthScript : MonoBehaviour
{
    [Tooltip("TotalHealth")]
    public float MaxHealthPoints = 200;
    public float CurrentHealthPoints = 200;
    [Space]
    [Tooltip("MaxStamina")]
    public float MaxStaminaPoints = 300;
    public float CurrentStaminaPoints = 300;
    [Tooltip("Variable of Stamina Loss while Sprinting per second")]
    public float StaminaSprintLoss = 50.0f;
    [Tooltip("Amound of stamina that is regenerated every Tick of Stamina Regen Frequency")]
    public float StaminaRegenRate = 0.1f;
    [Tooltip("Amount of stamina Regen Gained from points in endurance")]
    public float EduranceBonusPercentage = 10;
    [Tooltip("How Long it takes for StaminaRegenRate to occur")]
    public float StaminaRegenFrequency = 0.005f;
    [Tooltip("Time it takes after sprinting for stamina regen to begin")]
    public float StaminaInitialRegenTimer = 3;
    float BonusStamRegen = 1;
    float BaseStaminaRegenRate = 0.1f;
    [Space]
    [Tooltip("MaxMana")]
    public float MaxManaPoints = 100;
    public float CurrentManaPoints = 100;
    
    [Space]

    [Tooltip("Is this Character Bleeding")]
    public bool isBleeding = false;
    //AfflictBleed
    [Tooltip("Total Damage Bleed will Deal")]
    public float TotalBleedDamage;
    [HideInInspector]
    public float BleedFrequency;
    [HideInInspector]
    public int RoundsBleeding;
    float TempTotalBleed;
    float BleedIntervalDamage;
    float BleedRoundNumb;

    [Space]

    [Tooltip("Is this Character Poisoned")]
    public bool isPoisoned = false;
    //AfflictPoison
    [Tooltip("Total Damage Poison Will Deal")]
    public float TotalPoisonDamage;
    [HideInInspector]
    public float PoisonFrequency;
    [HideInInspector]
    public int RoundsPoisoned;
    float TempTotalPoison;
    float PoisonIntervalDamage;
    float PoisonRoundRumb;

    [Space]
    [Tooltip("Physical Resistance Percentage")]
    public float PhysicalResistanceModifier;
    [Tooltip("Magic Resistance Percentage(Not Implemented)")]
    public float MagicResistanceModifier;
    [Tooltip("Status Resistance Percentage")]
    public float StatusResistanceModifier;


    bool Yeet = false;
    float Countdown = 0;
    float StaminaCountdown = 0;
    float StaminaRegenProCountdown = 0;
    GameObject ScriptBoard;
    CharacterStatScipt PlayerStats;
    GameObject HealthBarObj;
    HealthBar playerHealthBar;
    HealthBarEnemy EnemyHealthBar;
    GameObject StaminaBarObj;
    StaminaBar staminaBar;
    PlayerMovenment playerMovenment;

    void Start()
    {
        ScriptBoard = GameObject.FindWithTag("ScriptBoard");
        if (gameObject.tag == "Enemy")
        {
            EnemyHealthBar = gameObject.GetComponent<HealthBarEnemy>();
            EnemyHealthBar.SetMaxHealth(MaxHealthPoints);
            EnemyHealthBar.SetHealth(CurrentHealthPoints);
        }

        if (gameObject.tag == "Player")
        {
            PlayerStats = ScriptBoard.GetComponent<CharacterStatScipt>();

            StaminaBarObj = GameObject.FindWithTag("StaminaBar");
            staminaBar = StaminaBarObj.GetComponent<StaminaBar>();

            HealthBarObj = GameObject.FindWithTag("HealthBar");
            playerHealthBar = HealthBarObj.GetComponent<HealthBar>();

            playerMovenment = gameObject.GetComponent<PlayerMovenment>();

            CurrentHealthPoints = MaxHealthPoints;
            CurrentManaPoints = MaxManaPoints;
            CurrentStaminaPoints = MaxStaminaPoints;

        }

    }
    public void TakeDamage(float AtackDamage)
    {
        float PhysicalResistance = AtackDamage * (PhysicalResistanceModifier / 100);
        CurrentHealthPoints -= (AtackDamage - PhysicalResistance);
        Debug.Log(AtackDamage - PhysicalResistance + " Enemy Damage Taken");


    }
    // Update is called once per frame
    void Update()
    {
        if (gameObject.tag == "Player")
        {
            
            if (MaxHealthPoints != PlayerStats.MaxHealth )
            {
                MaxHealthPoints = PlayerStats.MaxHealth;
            }
            if (MaxManaPoints != PlayerStats.MaxMana)
            {
                MaxManaPoints = PlayerStats.MaxMana;
            }
            if (MaxStaminaPoints != PlayerStats.MaxStamina)
            {
                MaxStaminaPoints = PlayerStats.MaxStamina;
            }
            if (CurrentHealthPoints >= MaxHealthPoints)
            {
                CurrentHealthPoints = MaxHealthPoints; 
            }
            if (CurrentManaPoints >= MaxManaPoints)
            {
                CurrentManaPoints = MaxManaPoints;
            }
            if (CurrentStaminaPoints >= MaxStaminaPoints)
            {
                CurrentStaminaPoints = MaxStaminaPoints;
            }


            playerHealthBar.SetMaxHealth(MaxHealthPoints);
            playerHealthBar.SetHealth(CurrentHealthPoints);
            staminaBar.SetMaxStamina(MaxStaminaPoints);
            staminaBar.SetStamina(CurrentStaminaPoints);
            PhysicalResistanceModifier = PlayerStats.PhysicalResistence;
            MagicResistanceModifier = PlayerStats.MagicResistence;
            StatusResistanceModifier = PlayerStats.StatusResistence;

            BonusStamRegen = (BaseStaminaRegenRate) * (PlayerStats.PlayerEndurance * (EduranceBonusPercentage / 100));
            StaminaRegenRate = (BaseStaminaRegenRate + BonusStamRegen);

            if (playerMovenment.IsSprinting)
            {
                StaminaCountdown = 0;
                float Variable = 0;
                Variable += Time.deltaTime;

                StaminaLoss(Variable * StaminaSprintLoss);
                
            }

            StaminaRegen();

        }
        if (gameObject.tag == "Enemy")
        {
            EnemyHealthBar = gameObject.GetComponent<HealthBarEnemy>();
            EnemyHealthBar.SetMaxHealth(MaxHealthPoints);
            EnemyHealthBar.SetHealth(CurrentHealthPoints);
        }

        if (isBleeding)
        {
            Bleeding();
        }
        if (isPoisoned)
        {
            Poisoned();

        }
        if (CurrentHealthPoints <= 0 && tag != "Player")
        {

            RemoveStatus();

            Vector3 Velocity = new Vector3(0, 20, 0 );
            Vector3 Rotation = new Vector3(10, 0, 10);
            CurrentHealthPoints = 0;
            Rigidbody RB;
            RB = gameObject.GetComponent<Rigidbody>();
            RB.constraints = RigidbodyConstraints.None;

            if (Yeet == false)
            {
                RB.AddForce(Velocity, ForceMode.Impulse);
                RB.AddTorque(Rotation, ForceMode.Impulse);

                Yeet = true;
            }
        }
        if (CurrentHealthPoints <= 0 && tag == "Player")
        {
            RemoveStatus();
            RagDollGameobject();
            
        }

        
    }

    void Bleeding()
    {
        float StatusResistance = TotalBleedDamage * (StatusResistanceModifier / 100);
        if (BleedRoundNumb == 0)
        {
            TempTotalBleed = TotalBleedDamage - StatusResistance;
        }
        BleedIntervalDamage = TotalBleedDamage / RoundsBleeding;

        Countdown += Time.deltaTime;
        

        if (Countdown >= BleedFrequency)
        {
            BleedRoundNumb++;
            Debug.Log(BleedRoundNumb + " Bleed Round");
            Debug.Log(BleedIntervalDamage + " Bleed Amount");
            CurrentHealthPoints -= BleedIntervalDamage;
            TempTotalBleed -= BleedIntervalDamage;
            Debug.Log(BleedFrequency + "Second/Seconds");
            Countdown = 0;
            
            if (TempTotalBleed <= 0)
            {
                isBleeding = false;
                BleedRoundNumb = 0;
                
                Debug.Log("Bleeding Stopped");
            }

        }
        
        if (TempTotalBleed <= 0)
        {
            TotalBleedDamage = 0;
        }
    }
    public void StackBleed()
    {
        TotalBleedDamage -= BleedIntervalDamage * (BleedRoundNumb);
        BleedRoundNumb = 0;
    }
    void Poisoned()
    {
        float StatusResistance = TotalPoisonDamage * (StatusResistanceModifier / 100);
        if (PoisonRoundRumb == 0)
        {
            TempTotalPoison = TotalPoisonDamage - StatusResistance;
        }
        PoisonIntervalDamage = TotalPoisonDamage / RoundsPoisoned;

        Countdown += Time.deltaTime;


        if (Countdown >= PoisonFrequency)
        {
            PoisonRoundRumb++;
            Debug.Log(PoisonRoundRumb + " Poisoned Round");
            Debug.Log(PoisonIntervalDamage + " Poison Damage Amount");
            CurrentHealthPoints -= PoisonIntervalDamage;
            TempTotalPoison -= PoisonIntervalDamage;
            Debug.Log(PoisonFrequency + "Second/Seconds");
            Countdown = 0;

            if (TempTotalPoison <= 0)
            {
                isPoisoned = false;
                PoisonRoundRumb = 0;

                Debug.Log("Poison Stopped");
            }

        }

        if (TempTotalPoison <= 0)
        {
            TotalPoisonDamage = 0;
        }
    }
    public void StackPoison()
    {
        PoisonRoundRumb = 0;
    }
    public void RemoveStatus()
    {
        isBleeding = false;
        isPoisoned = false;
        TotalBleedDamage = 0;
        TotalPoisonDamage = 0;
        
    }
    public void StaminaLoss(float Stamina)
    {
        CurrentStaminaPoints -= Stamina;
        if (CurrentStaminaPoints <= 0)
        {
            CurrentStaminaPoints = 0;
            playerMovenment.CanSprint = false;
        }
        else if (CurrentStaminaPoints > 0)
        {
            playerMovenment.CanSprint = true;
        }
    }
    public void StaminaRegen()
    {

        if (playerMovenment.IsSprinting == false)
        {

            if (CurrentStaminaPoints > MaxStaminaPoints)
            {
                CurrentStaminaPoints = MaxStaminaPoints;
            }
            StaminaCountdown += Time.deltaTime;
            StaminaRegenProCountdown += Time.deltaTime;

            if (CurrentStaminaPoints >= 0)
            {
                
                if (StaminaCountdown > StaminaInitialRegenTimer)
                {
                    if (StaminaRegenProCountdown > StaminaRegenFrequency)
                    {
                        CurrentStaminaPoints += StaminaRegenRate;
                        StaminaRegenProCountdown = 0;
                    }
                    
                }
            }

        }
    }

    void RagDollGameobject()
    {
        Vector3 Velocity = new Vector3(0, 20, 0);
        Vector3 Rotation = new Vector3(10, 10, 10);
        CurrentHealthPoints = 0;
        PlayerMovenment playerMovenment;
        CharacterController characterController;
        MouseFollow mouseFollow;
        CapsuleCollider capsuleCollider;
        playerMovenment = GetComponent<PlayerMovenment>();
        playerMovenment.enabled = false;

        characterController = GetComponent<CharacterController>();
        characterController.enabled = false;

        mouseFollow = gameObject.GetComponentInChildren<MouseFollow>();
        mouseFollow.enabled = false;

        capsuleCollider = gameObject.GetComponent<CapsuleCollider>();
        capsuleCollider.enabled = true;

        Rigidbody RB;




        if (Yeet == false)
        {

            RB = gameObject.AddComponent<Rigidbody>();
            RB = gameObject.GetComponent<Rigidbody>();
            RB.constraints = RigidbodyConstraints.None;
            RB.AddForce(Velocity, ForceMode.Impulse);
            RB.AddTorque(Rotation, ForceMode.Impulse);

            Yeet = true;
        }
    }
    

}
