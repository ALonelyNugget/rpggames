﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCWeapons : MonoBehaviour
{
    [Header("Weapon Base Attack Damage")]
    [Tooltip("Base Attack Damage of Weapon")]
    public float WeaponDamage = 20;
    [Tooltip("Crit Chance %")]
    public float CritChance = 2.0f;
    //Variable for dealing damage
    float DamageDelt = 0;

    [Header("Weapon Status Afflictions")]
    [Space]
    //AfflictBleed
    [Tooltip("Will Your Attack Inflict Bleed")]
    public bool AfflictBleed = false;
    [Tooltip("How Often The Enemy will Take Damage in seconds")]
    public float BleedFrequency = 3.0f;
    [Tooltip("Amount of Bleed Damage intervals")]
    public int RoundsBleeding = 4;
    [Tooltip("Percentage Amound of Max Damage That will Be Inflicted as Bleed")]
    public float BleedModifier = 0.2f;
    float BleedDamage;

    [Space]
    //AfflictPoison
    [Tooltip("Will Your Attack Inflict poison")]
    public bool AfflictPoison = false;
    [Tooltip("Total Damage Poison Will Deal")]
    public float PoisonDamage = 20;
    [Tooltip("How Often The Enemy will Take Damage in seconds")]
    public float PoisonFrequency = 0.05f;
    [Tooltip("Amount of Poison Damage intervals")]
    public int RoundsPoisoned = 400;

    [Space]
    //Damage Bonus
    public bool Magic = false;

    [Space]
    DummyHealthScript Enemy;
    private void OnTriggerEnter(Collider other)
    {
        Enemy = other.GetComponent<DummyHealthScript>();
        if (other.tag != "Player")
        {
            return;
        }
        else
        {
            if (CheckCrit())
            {
                Enemy.TakeDamage(WeaponDamage * 2);
                Debug.Log((WeaponDamage * 2) + " Crit Damage Delt");
                DamageDelt = WeaponDamage * 2;
            }
            else
            {
                Enemy.TakeDamage(WeaponDamage);
                Debug.Log((WeaponDamage) + " Damage Delt");
                DamageDelt = WeaponDamage;
            }   
        }

        CheckAfflictedStatus();
    }
    void CheckAfflictedStatus()
    {
        if (AfflictBleed == true)
        {
            BleedDamage = DamageDelt * BleedModifier;
            if (Enemy.isBleeding == false)
            {
                Enemy.TotalBleedDamage += BleedDamage;
            }
            else if (Enemy.isBleeding)
            {
                Enemy.StackBleed();
                Enemy.TotalBleedDamage += BleedDamage;

            }
            Enemy.BleedFrequency = BleedFrequency;
            Enemy.RoundsBleeding = RoundsBleeding;
            Enemy.isBleeding = true;
        }
        else
        {
            Debug.Log("No Bleed Status Attack");
        }
        if (AfflictPoison == true)
        {

            if (Enemy.isPoisoned == false)
            {
                Enemy.TotalPoisonDamage += PoisonDamage;
            }
            else if (Enemy.isPoisoned)
            {
                Enemy.StackPoison();
                Enemy.TotalPoisonDamage += PoisonDamage;

            }
            Enemy.PoisonFrequency = PoisonFrequency;
            Enemy.RoundsPoisoned = RoundsPoisoned;
            Enemy.isPoisoned = true;
        }
        else
        {
            Debug.Log("No Poison Status Attack");
        }
    }
    bool CheckCrit()
    {
        float Rand;
        Rand = Random.Range(0.0f, 100.0f);
        Debug.Log(Rand + " Number Rolled " + CritChance + " Current CritChance");

        return (Rand <= CritChance);


    }
}
