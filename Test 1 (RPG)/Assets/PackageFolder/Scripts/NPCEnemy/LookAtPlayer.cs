﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{

    GameObject target1;
    Transform target2;

    void Update()
    {

        target1 = GameObject.FindWithTag("MainCamera");
        target2 = target1.transform;

        transform.LookAt(target2);

    }
}
