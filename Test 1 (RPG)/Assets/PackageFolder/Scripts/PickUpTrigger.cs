﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpTrigger : MonoBehaviour
{
    public bool ExpPickup = false;
    public float Exp = 2000;
    [Space]
    public bool HealthPickup = false;
    public float HealTotal = 100;
    [Space]
    public bool ManaPickup = false;
    public float ManaRegen = 50;
    [Space]
    [Tooltip("This Removes Status Effects")]
    public bool Cleanse = false;
    
    [Space]

    CharacterStatScipt PlayerStats;
    DummyHealthScript Player;
    public GameObject ScriptBoard;
    void Start()
    {
        PlayerStats = ScriptBoard.GetComponent<CharacterStatScipt>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            Player = other.GetComponent<DummyHealthScript>();

        if (other.tag == "Player" && ExpPickup == true)
        {

            

            PlayerStats.CurrentXP += Exp;
        }
        if (other.tag == "Player" && HealthPickup == true)
        {
            Player.CurrentHealthPoints += HealTotal;
        }
        if (other.tag == "Player" && ManaPickup == true)
        {

            Player.CurrentManaPoints += ManaRegen;
        }
        if (other.tag == "Player" && Cleanse == true)
        {
            Player.TotalPoisonDamage = 0.0f;
            Player.isPoisoned = false;
            Player.TotalBleedDamage = 0.0f;
            Player.isBleeding = false;
        }

    }
}
